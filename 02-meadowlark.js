const express = require('express')
const expressHandlebars = require('express-handlebars')

const app = express()

// configure Handlebars view engine
app.engine('handlebars', expressHandlebars({
  defaultLayout: 'main',
}))
app.set('view engine', 'handlebars')

app.use(express.static(__dirname + '/public'))

const port = process.env.PORT || 3000

app.get('/', (req, res) => res.status(200).render('home.handlebars'))
app.get('/about', (req, res) => res.status(200).render('about.handlebars'))
app.use((req, res) => res.status(404).render('404.handlebars'))

// custom 500 page
app.use((err, req, res, next) => {
  console.error(err.message)
  res.status(500).render('500.handlebars')
})

app.listen(port, () => console.log(
  `Express started on http://localhost:${port}; ` +
  `press Ctrl-C to terminate.`))
